# SlimeSwing - Platformer game written in Java

SlimeSwing platformer game written in Java using Slick2D,MarteEngine and
a simple JSON library.

## Table of Contents
* [General Info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [License](#license)
* [Acknowledgments](#acknowledgments)

## General Info
The aim of the game is to get the character to the exit point of the map, while
collecting as many points as possible. Movement is similar to that of Angry Birds,
the difference being that the player can launch the character multiple times,
with a cooldown inbetween launches. There are 6 levels, each unlocked by completing
the previous one.  

## Screenshots
![Title Screen](./res/screenshots/title_screen.png)  
![Level Select](./res/screenshots/level_select.png)  
![Level 2](./res/screenshots/level2.png)  

## Technologies
* Java
* [Slick2D build 237](http://slick.ninjacave.com)
* [MarteEngine 0.3](https://github.com/Gornova/MarteEngine)
* [json-simple 1.1.1](https://github.com/fangyidong/json-simple/)

## License
[MIT](https://choosealicense.com/licenses/mit/)  
All of the used assets are royalty free.

## Acknowledgments
* To everyone whose code was used
* [FrumsNL](https://www.youtube.com/user/frumsnl) Slick2D tutorial