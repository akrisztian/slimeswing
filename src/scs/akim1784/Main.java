package scs.akim1784;

import scs.akim1784.states.*;
import scs.akim1784.util.*;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;

/**
 * Main class initializes Manager classes, GameContainer, States and adds states to GameContainer
 *
 * @author Antal Krisztian-Tamas
 * @version 0.1
 * @see org.newdawn.slick
 * @see it.marteEngine
 */
public class Main extends StateBasedGame {

    public Main(String name) {
        super(name);
    }

    public static void main(String[] args) {
        AppGameContainer appGameContainer;

        try{
            Main game = new Main("SlimeSwing");
            appGameContainer = new AppGameContainer(game);
            try {
                new OptionsManager(appGameContainer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            appGameContainer.setDisplayMode(GameWindow.WIDTH,GameWindow.HEIGHT,false);
            appGameContainer.setShowFPS(true);
            appGameContainer.start();
        }catch (SlickException e){
            e.printStackTrace();
        }
    }

    @Override
    public void initStatesList(GameContainer gameContainer) {


        try {
            new SaveManager();
        } catch (IOException e) {
            e.printStackTrace();
        }

        new ResourceManager();

        new MusicManager(this).start();

        addState(new MenuState(States.MENU,gameContainer));
        addState(new OptionState(States.OPTIONS,gameContainer));
        addState(new LevelSelectState(States.LEVEL_SELECT,gameContainer,this));

    }

}
