package scs.akim1784.util;

import java.io.*;

/**
 * SaveManager class, used for storing the current state of a game, and loading it from a save.ini file
 * Structure of save.ini:
 * 1.               Current unlocked level
 * 2...LEVEL_COUNT.  HighScore for each individual level
 */
public class SaveManager {

    public static final int LEVEL_COUNT = 6;
    public static int[] scores;
    public static int level;
    public static int totalScore;
    private static File saveFile;

    public SaveManager() throws IOException {

        saveFile = new File("opt/save.ini");
        scores=new int[LEVEL_COUNT];

        if(!saveFile.exists()){
            saveFile.createNewFile();

            level=1;
            totalScore=0;

            saveGame();
        }else{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(saveFile));

            level=Integer.parseInt(bufferedReader.readLine());
            totalScore=0;

            String line;

            for(int i=0; i<LEVEL_COUNT; i++){
                line=bufferedReader.readLine();
                scores[i]=Integer.parseInt(line);
                totalScore+=scores[i];
            }
        }


    }

    /**
     * Writes current unlocked level and all level scores to save.ini file
     */
    public static void saveGame(){

        totalScore=0;

        try(PrintWriter printWriter = new PrintWriter(saveFile)) {
            printWriter.write(level+"\n");

            for(int i=0;i<LEVEL_COUNT;i++){
                totalScore+=scores[i];
                printWriter.write(scores[i] + "\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * deletes current save and save.ini file
     * @throws IOException          upon file access failure
     */
    public static void deleteSave() throws IOException {

        saveFile.delete();
        saveFile.createNewFile();

        level=1;
        totalScore=0;

        for(int i=0;i<LEVEL_COUNT;i++){
            scores[i]=0;
        }

        saveGame();
    }
}
