package scs.akim1784.util;

import org.newdawn.slick.*;

import java.util.HashMap;
import java.util.Map;

/**
 * ResourceManager class used to load and access all game resources (images,sprites,music)
 */
public class ResourceManager {

    private static Map<String, Image> images;
    private static Map<String, SpriteSheet> sprites;
    private static Map<String, Music> music;

    public ResourceManager() {
        images= new HashMap<>();
        sprites= new HashMap<>();
        music= new HashMap<>();

        try {
            sprites.put("tileset", loadSpriteSheet("res/tilesets/tileset.png", TileSize.SIZE, TileSize.SIZE));
            sprites.put("character", loadSpriteSheet("res/tilesets/character2.png", TileSize.SIZE, TileSize.SIZE));
            images.put("logo",loadImage("/res/logo.png"));
            images.put("button",loadImage("res/button.png"));
            images.put("locked",loadImage("res/locked.png"));
            music.put("menu",loadMusic("res/sounds/menu.ogg"));
            music.put("game",loadMusic("res/sounds/game.ogg"));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    private Image loadImage(String path) throws SlickException {
        return new Image(path,false,Image.FILTER_NEAREST);
    }

    private SpriteSheet loadSpriteSheet(String path, int tileWidth, int tileHeight) throws SlickException {
        return new SpriteSheet(loadImage(path),tileWidth,tileHeight);
    }

    private Music loadMusic(String path) throws SlickException {
        return new Music(path);
    }

    public static Image getSprite(String getter,int x, int y){
        return sprites.get(getter).getSubImage(x,y);
    }

    public static SpriteSheet getSpriteSheet(String getter){
        return sprites.get(getter);
    }

    public static Image getImage(String getter){
        return images.get(getter);
    }

    public static Music getMusic(String getter) {
        return music.get(getter);
    }


}
