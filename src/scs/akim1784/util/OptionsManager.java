package scs.akim1784.util;

import org.newdawn.slick.GameContainer;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * OptionsManager class used to store, load and manage game options
 * Saves settings into options.ini which consists of (OptionName,State) pairs
 * Currently sound, VSync and gameSpeed options are implemented
 * Scaling does not work as of yet.
 */

public class OptionsManager {

    private static GameContainer gameContainer;
    private static File optionsFile;
    private static Map<String,String> setMap;

    public OptionsManager(GameContainer gameContainer) throws IOException {
        OptionsManager.gameContainer = gameContainer;

        optionsFile = new File("opt/game.ini");
        setMap = new HashMap<>();

        if(!optionsFile.exists()){
            optionsFile.createNewFile();

            try(PrintWriter out = new PrintWriter(optionsFile)){
                out.write("spd 1\nscl 2\nvsync 1\nsnd 0");
            }catch (FileNotFoundException e){
                e.printStackTrace();
            }

        }

        loadSetting();
    }

    /**
     * Loads settings from game.ini file
     * @throws IOException          upon file read failure
     */
    private void loadSetting() throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader(optionsFile));

        String line;
        while((line = bufferedReader.readLine()) != null){
            String[] words = line.split(" ");
            setMap.put(words[0],words[1]);

            switch (words[0]){
                case "spd":
                    if(words[1].equals("1")){
                        gameContainer.setTargetFrameRate(30);
                    }else{
                        gameContainer.setTargetFrameRate(60);
                    }
                    break;
                case "scl":
                    if(words[1].equals("1")){
                        TileSize.SCALED_SIZE=16;
                        TileSize.SCALE=TileSize.SCALED_SIZE/TileSize.SIZE;
                    }else{
                        TileSize.SCALED_SIZE=32;
                        TileSize.SCALE=TileSize.SCALED_SIZE/TileSize.SIZE;
                    }
                    break;
                case "vsync":
                    if(words[1].equals("0")){
                        gameContainer.setVSync(false);
                    }else{
                        gameContainer.setVSync(true);
                    }
                    break;
                case "snd":
                    break;
            }
        }
        gameContainer.setAlwaysRender(true);
    }

    /**
     * Reloads settings based on current settingMap entries
     */
    public static void reloadSettings() {
        for(Map.Entry<String,String> entry : setMap.entrySet()){
            switch (entry.getKey()){
                case "spd":
                    if(entry.getValue().equals("1")){
                        gameContainer.setTargetFrameRate(30);
                    }else{
                        gameContainer.setTargetFrameRate(60);
                    }
                    break;
                case "scl":
                    if(entry.getValue().equals("1")){
                        //TileSize.SCALED_SIZE=16;
                        //TileSize.SCALE=TileSize.SCALED_SIZE/TileSize.SIZE;
                    }else{
                        //TileSize.SCALED_SIZE=32;
                        //TileSize.SCALE=TileSize.SCALED_SIZE/TileSize.SIZE;
                    }
                    break;
                case "vsync":
                    if(entry.getValue().equals("0")){
                        gameContainer.setVSync(false);
                    }else{
                        gameContainer.setVSync(true);
                    }
                    break;
                case "snd":
                    break;
            }
        }
    }

    public static String getSpeed(){
        return setMap.get("spd");
    }

    public static void setSpeed(String speed){
        setMap.replace("spd",speed);
    }

    public static String getScale(){
        return setMap.get("scl");
    }

    public static void setScale(String scale){
        setMap.replace("scl",scale);
    }

    public static String getVSync(){
        return setMap.get("vsync");
    }

    public static void setVSync(String vSync){
        setMap.replace("vsync",vSync);
    }

    public static String getSound(){
        return setMap.get("snd");
    }

    public static void setSound(String sound){
        setMap.replace("snd",sound);
    }

    /**
     * Saves current options to game.ini file
     */
    public static void saveOptions(){
        try(PrintWriter printWriter = new PrintWriter(optionsFile)) {
            for(Map.Entry<String,String> entry : setMap.entrySet()){
                printWriter.write(entry.getKey() + " " + entry.getValue()+"\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
