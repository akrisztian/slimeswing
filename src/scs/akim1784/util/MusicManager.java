package scs.akim1784.util;

import scs.akim1784.states.States;
import org.newdawn.slick.state.StateBasedGame;


/**
 * MusicManager class which checks for current gameState and alters the music accordingly,
 * if sound is turned on in OptionsManager
 */
//TODO: Choose random song every level
public class MusicManager extends Thread {
    private StateBasedGame game;
    private volatile boolean running = true;

    public MusicManager(StateBasedGame game){
        this.game = game;
    }
    @Override
    public void run() {
        super.run();

        try{
            sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while(running){
            int ID = game.getCurrentStateID();

            if(OptionsManager.getSound().equals("1")) {
                if (ID == States.GAME) {
                    if (ResourceManager.getMusic("menu").playing()) {
                        ResourceManager.getMusic("menu").stop();
                    }
                    if (!ResourceManager.getMusic("game").playing()) {
                        ResourceManager.getMusic("game").play();
                        ResourceManager.getMusic("game").loop();
                    }
                } else {
                    if (ResourceManager.getMusic("game").playing()) {
                        ResourceManager.getMusic("game").stop();
                    }
                    if (!ResourceManager.getMusic("menu").playing()) {
                        ResourceManager.getMusic("menu").play();
                        ResourceManager.getMusic("menu").loop();
                    }
                }
            }else{
                ResourceManager.getMusic("menu").stop();
                ResourceManager.getMusic("game").stop();
            }
        }
    }
}
