package scs.akim1784.util;

/**
 * TileSize class used to easily reference tile image sizes and scale
 */
public class TileSize {
    //TODO: Fix Game Scaling from options menu
    public static int SCALED_SIZE = 32;
    public static final int SIZE = 16;  //Size of tiles used in tileset/maps
    public static int SCALE = SCALED_SIZE/SIZE;
}
