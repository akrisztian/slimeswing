package scs.akim1784.util;

import scs.akim1784.entities.tiles.*;
import it.marteEngine.World;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.StateBasedGame;

import java.io.FileReader;
import java.io.IOException;

/**
 * LevelLoader class used to load json Tiled Maps, parse them, and then render the loaded map.
 */
public class LevelLoader {

    public static Image[][] solids;
    public static Image[][] bounce;
    public static Image[][] spawns;
    public static Image[][] hazards;
    public static Image[][] backgrounds;
    public static Image[][] exits;
    public static Image[][] start;
    public static int HEIGHT;
    public static int WIDTH;
    public static int startX;
    public static int startY;

   public static void render(float xRender, float yRender, World gameState, StateBasedGame game){

        int offset=2;
        int xStart = (int) (xRender/ TileSize.SCALED_SIZE)-offset;
        int yStart = (int) (yRender/ TileSize.SCALED_SIZE)-offset;
        int xEnd = (GameWindow.WIDTH/ TileSize.SCALED_SIZE) + xStart + (offset*2);
        int yEnd = (GameWindow.HEIGHT/ TileSize.SCALED_SIZE) + yStart + (offset*2);

        for(int x=xStart; x<xEnd; x++){
            for(int y=yStart; y<yEnd; y++){
                if(backgroundTile(x,y)){
                    gameState.add(new Background(x* TileSize.SCALED_SIZE,y* TileSize.SCALED_SIZE, backgrounds[x][y]));
                }
                if(solidTile(x,y)){
                    gameState.add(new Wall(x* TileSize.SCALED_SIZE,y* TileSize.SCALED_SIZE, solids[x][y]));
                }
                if(hazardTile(x,y)){
                    gameState.add(new Hazard(x*TileSize.SCALED_SIZE,y*TileSize.SCALED_SIZE, hazards[x][y]));
                }
                if(spawnTile(x,y)){
                    gameState.add(new Spawn(x* TileSize.SCALED_SIZE,y* TileSize.SCALED_SIZE, spawns[x][y],getColorCode(spawns[x][y].getColor(7,7))));
                }
                if(exitTile(x,y)){
                    gameState.add(new Exit(x*TileSize.SCALED_SIZE,y*TileSize.SCALED_SIZE, exits[x][y],game,gameState));
                }
                if(startTile(x,y)){
                    startX=x;
                    startY=y;
                }
                if(bounceTile(x,y)){
                    gameState.add(new BounceWall(x*TileSize.SCALED_SIZE,y*TileSize.SCALED_SIZE,bounce[x][y]));
                }
            }
        }

    }

    /**
     * Loads file, parses file, and distributes tiles into arrays, based on map layers
     * @param path                  .json map file path
     * @throws IOException          upon file open failure
     * @throws ParseException       upon file parse failure
     */
    public static void load(String path) throws IOException, ParseException {

        JSONParser parser= new JSONParser();
        Object obj = parser.parse(new FileReader(path));
        JSONObject jsonObject = (JSONObject) obj;

        JSONArray layers = (JSONArray) jsonObject.get("layers");
        int amount=layers.size();

        for(int i=0; i<amount; i++){
            JSONObject layer = (JSONObject) layers.get(i);
            String type = (String) layer.get("name");
            WIDTH= (int) ((long)layer.get("width"));
            HEIGHT= (int) ((long)layer.get("height"));
            if(type.equals("background")){
                backgrounds=parse((JSONArray)layer.get("data"));
            }else if(type.equals("hazards")){
                hazards=parse((JSONArray)layer.get("data"));
            }else if(type.equals("solids")){
                solids=parse((JSONArray)layer.get("data"));
            }else if(type.equals("spawns")){
                spawns=parse((JSONArray) layer.get("data"));
            }else if(type.equals("exits")){
                exits=parse((JSONArray) layer.get("data"));
            }else if(type.equals("start")){
                start=parse((JSONArray) layer.get("data"));
            }else if(type.equals("bounce")){
                bounce=parse((JSONArray) layer.get("data"));
            }
        }

    }

    /**
     * Loads sprites into arrays based on a map layer's "data" field
     * @param arr           Layer's "data" field
     * @return              Image[][] containing all tiles in given layer
     */
    private static Image[][] parse(JSONArray arr){
        Image[][] layer=new Image[WIDTH][HEIGHT];
        int index;

        for(int y=0; y < WIDTH; y++){
            for(int x=0; x < HEIGHT; x++){
                index = (int) ((long)arr.get((y*WIDTH)+x));
                layer[x][y]=getSpriteImage(index);
            }
        }

        return layer;
    }

    /**
     * Converts "data" array xy index to [x][y] indices and returns corresponding tile image from tileset
     * @param index         "data" array xy index
     * @return              corresponding [x][y] image from tileset
     */
    private static Image getSpriteImage(int index){
        if(index==0){
            return null;
        }

        index=index-1;

        SpriteSheet sheet = ResourceManager.getSpriteSheet("tileset");
        int vertical=sheet.getVerticalCount();
        int horizontal=sheet.getHorizontalCount();

        int y=(index/vertical);
        int x=(index%horizontal);

        return sheet.getSubImage(x,y);

    }

    /**
     * Returns a number based on tile color. Used to differentiate Spawn entities by color
     * @param color         Color of the Spawn tile
     * @return              Spawn entity score multiplier in {1,2,3,4}
     */
    private static int getColorCode(Color color){
       if(color.getBlue()==62){
           return 1;
       }
       else if(color.getBlue()==222){
           return 2;
       }else if(color.getBlue()==10){
           return 3;
       }else if(color.getBlue()==42){
           return 4;
       }

       return 1;
    }

    /**
     *
     * @param x         x coordinate
     * @param y         y coordinate
     * @return          True if tile is in bounds of layer
     */
    public static boolean inBounds(int x, int y){
        return(x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT);
    }

    public static boolean solidTile(int x, int y){
        return (inBounds(x,y) && solids[x][y]!=null);
    }

    public static boolean spawnTile(int x, int y){
        return (inBounds(x,y) && spawns[x][y]!=null);
    }

    public static boolean backgroundTile(int x, int y){
        return (inBounds(x,y) && backgrounds[x][y]!=null);
    }

    public static boolean hazardTile(int x, int y){
       return (inBounds(x,y) && hazards[x][y]!=null);
    }

    public static boolean exitTile(int x, int y){
       return (inBounds(x,y) && exits[x][y]!=null);
    }

    public static boolean startTile(int x, int y){
       return (inBounds(x,y) && start[x][y]!=null);
    }

    public static boolean bounceTile(int x, int y){
       return (inBounds(x,y) && bounce[x][y]!=null);
    }
}
