package scs.akim1784.util;

/**
 * GameWindow class used to easily reference window proportions
 */
public class GameWindow {
    public static final int WIDTH=400* TileSize.SCALE;
    public static final int HEIGHT=400* TileSize.SCALE;
}
