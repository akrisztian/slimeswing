package scs.akim1784.entities;

import scs.akim1784.util.TileSize;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.*;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 * Button entity, has a switchState to distinguish action, and centered text.
 * @see it.marteEngine.entity.Entity
 */

public class Button extends Entity {

    protected String text;
    private int switchState;
    protected double cordX;
    protected double cordY;


    public Button(float x, float y, Image image, String text, int switchState) throws SlickException {
        super(x, y, image);

        addType("BUTTON");
        scale=TileSize.SCALE;
        setHitBox(0,0,TileSize.SCALED_SIZE*6,TileSize.SCALED_SIZE*3);

        this.text=text;
        this.switchState = switchState;
        this.depth=switchState;

        this.getCurrentImage().setName(text);
        scale= TileSize.SCALE;
    }

    /**
     * Draw @param text centered on the button
     * @param container             GameContainer context
     * @param g                     Graphics
     * @throws SlickException       if draw fails
     */
    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        super.render(container, g);

        g.scale((float)TileSize.SCALE/2,(float)TileSize.SCALE/2);
        int width = g.getFont().getWidth(text)/(32/TileSize.SCALED_SIZE);
        int height = g.getFont().getHeight(text)/(32/TileSize.SCALED_SIZE);
        cordX = (int) ((x + (hitboxWidth - width)/2)*(32/TileSize.SCALED_SIZE));
        cordY = (int) ((y + (hitboxHeight - height)/2)*(32/TileSize.SCALED_SIZE));
        g.drawString(text,(float)cordX,(float)cordY);
        g.resetTransform();
    }
}
