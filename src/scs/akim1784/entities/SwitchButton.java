package scs.akim1784.entities;

import scs.akim1784.util.OptionsManager;
import org.newdawn.slick.*;

/**
 * SwitchButton entity, used in the Options menu. Upon clicking a SwitchButton entity the text swaps between two states
 * (ex. ON/OFF). On click it also updates the setting value in the OptionsManager
 * @see scs.akim1784.entities.Button
 * @see it.marteEngine.entity.Entity
 */

public class SwitchButton extends Button {

    public String[] states;

    public SwitchButton(float x, float y, Image image, String text, int switchState,String[] states) throws SlickException {
        super(x, y, image, text, switchState);
        this.states=states;

        bindToMouse("Click", Input.MOUSE_LEFT_BUTTON);
    }


    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        super.update(container, delta);
        String[] words=text.split(" ");

        //Check current settings
        switch (words[0]){
            case "Speed":
                if(OptionsManager.getSpeed().equals("1")){
                    text=words[0] + " " + "1x";
                }else{
                    text=words[0] + " " + "2x";
                }
                break;
            case "Scale":
                if(OptionsManager.getScale().equals("1")){
                    text=words[0] + " " + "1x";
                }else{
                    text=words[0] + " " + "2x";
                }
                break;
            case "VSync":
                if(OptionsManager.getVSync().equals("0")){
                    text=words[0] + " " + "OFF";
                }else{
                    text=words[0] + " " + "ON";
                }
                break;
            case "Sound":
                if(OptionsManager.getSound().equals("0")){
                    text=words[0] + " " + "OFF";
                }else{
                    text=words[0] + " " + "ON";
                }
                break;
        }

        //Update settings on mouse click
        int mX = container.getInput().getMouseX();
        int mY = container.getInput().getMouseY();

        if (mX >= x && mX <= x+hitboxWidth  && mY >= y && mY <= y + hitboxHeight) {
            if(pressed("Click")) {

                if(words[1].equals(states[0])){
                    text = words[0] + " " + states[1];
                }else{
                    text = words[0] + " " + states[0];
                }

                switch (words[0]){
                    case "Speed":
                        if(words[1].equals("1x")){
                            OptionsManager.setSpeed("2");
                        }else{
                            OptionsManager.setSpeed("1");
                        }
                        break;
                    case "Scale":
                        if(words[1].equals("2x")){
                            OptionsManager.setScale("1");
                        }else{
                            OptionsManager.setScale("2");
                        }
                        break;
                    case "VSync":
                        if(words[1].equals("ON")){
                            OptionsManager.setVSync("0");
                        }else{
                            OptionsManager.setVSync("1");
                        }
                        break;
                    case "Sound":
                        if(words[1].equals("ON")){
                            OptionsManager.setSound("0");
                        }else{
                            OptionsManager.setSound("1");
                        }
                        break;
                }
            }

        }

    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        super.render(container, g);
    }

    public String getText(){
        return text;
    }

    public void setText(String text){
        this.text=text;
    }
}
