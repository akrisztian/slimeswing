package scs.akim1784.entities.tiles;

import scs.akim1784.util.TileSize;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.Image;

/**
 * Background entity for displaying background tiles
 * @see it.marteEngine.entity.Entity
 */
public class Background extends Entity {
    public Background(float x, float y, Image image) {
        super(x, y, image);
        scale = TileSize.SCALE;
    }
}
