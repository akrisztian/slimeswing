package scs.akim1784.entities.tiles;

import scs.akim1784.util.TileSize;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;

/**
 * BounceWall entity used to distinguish Wall entities off of which a Player entity can bounce back.
 * @see it.marteEngine.entity.Entity
 */
public class BounceWall extends Entity {
    public BounceWall(float x, float y, Image image) {
        super(x, y, image);

        scale= TileSize.SCALE;
        setHitBox(0,0, TileSize.SCALED_SIZE, TileSize.SCALED_SIZE);
        setColor(Color.yellow);

        addType("BOUNCE");
    }
}
