package scs.akim1784.entities.tiles;

import scs.akim1784.util.TileSize;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.Image;

/**
 * Spawn entities are "points". Once the Player entity collides with a Spawn entity
 * the Spawn entity is destroyed and its value is added to the Player entity's score
 * depth is used to store the point multiplier of the spawn entity
 * @see it.marteEngine.entity.Entity
 */
public class Spawn extends Entity {

    public Spawn(float x, float y, Image image, int color) {
        super(x, y, image);
        setHitBox(0,0,16* TileSize.SCALE,16* TileSize.SCALE);
        scale = TileSize.SCALE;
        depth = color;
        addType("SPAWN");
    }
}
