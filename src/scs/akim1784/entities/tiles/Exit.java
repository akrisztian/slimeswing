package scs.akim1784.entities.tiles;

import scs.akim1784.states.GameState;
import scs.akim1784.states.States;
import scs.akim1784.util.SaveManager;
import scs.akim1784.util.TileSize;
import it.marteEngine.World;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Exit entity acts as an exit point to all levels. Once the Player entity collides with an Exit entity
 * the game is saved, and the user is taken to the Level Select screen
 * @see it.marteEngine.entity.Entity
 */
public class Exit extends Entity {

    private StateBasedGame game;
    private GameState gameState;

    public Exit(float x, float y, Image image, StateBasedGame game, World gameState) {
        super(x, y, image);
        this.game=game;
        this.gameState= (GameState) gameState;
        setHitBox(0,0,16* TileSize.SCALE,16*TileSize.SCALE);
        scale=TileSize.SCALE;

        addType("EXIT");
    }

    @Override
    public void collisionResponse(Entity other) {
        super.collisionResponse(other);
        try {
            changeLevel(game,gameState);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param game                      current game, used to change state
     * @param gameState                 needed to update the unlocked level in SaveManager
     * @throws InterruptedException     sleep used to ease the transition between gameStates
     */
    private void changeLevel(StateBasedGame game, GameState gameState) throws InterruptedException {
        if(gameState.getLevel() == SaveManager.level){
            SaveManager.level = gameState.getLevel()+1;
        }
        SaveManager.saveGame();

        gameState.removeAll(gameState.getEntities());
        Thread.sleep(500);
        game.enterState(States.LEVEL_SELECT);
    }
}
