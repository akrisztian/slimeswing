package scs.akim1784.entities.tiles;

import scs.akim1784.util.TileSize;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.Image;

/**
 * Wall entity used to represent walls that the Player entity collides with. The Player entity does not bounce back
 * off of basic Wall entities, it merely collides with them.
 * @see it.marteEngine.entity.Entity
 */
public class Wall extends Entity {
    public Wall(float x, float y, Image image) {
        super(x, y, image);
        scale= TileSize.SCALE;
        setHitBox(0,0, TileSize.SCALED_SIZE, TileSize.SCALED_SIZE);
        addType(SOLID);
        addType("WALL");
    }


}
