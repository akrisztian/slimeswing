package scs.akim1784.entities.tiles;

import scs.akim1784.util.TileSize;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.Image;

/**
 * Start entity acts as the Players spawn point in a Level. Upon Level start and upon the Player entity colliding
 * with a Hazard entity, the Player entity is placed to the position of the Start entity
 * @see it.marteEngine.entity.Entity
 */
public class Start extends Entity {
    public Start(float x, float y, Image image) {
        super(x, y, image);
        scale= TileSize.SCALE;
        setHitBox(0,0,16*TileSize.SCALE,16*TileSize.SCALE);

        addType("START");
    }
}
