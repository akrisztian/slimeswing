package scs.akim1784.entities.tiles;

import scs.akim1784.util.TileSize;
import it.marteEngine.entity.Entity;
import org.newdawn.slick.Image;

/**
 * Hazard entity used to represent stage hazards. Once the Player entity collides with a Hazard entity
 * the Player entity is placed to the starting point of the current level, and current score = 0
 * @see it.marteEngine.entity.Entity
 */
public class Hazard extends Entity {
    public Hazard(float x, float y, Image image) {
        super(x, y, image);
        scale = TileSize.SCALE;
        setHitBox(0,5*TileSize.SCALE, TileSize.SCALED_SIZE, TileSize.SCALED_SIZE);
        addType("HAZARD");
    }


}
