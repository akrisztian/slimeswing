package scs.akim1784.entities;

import scs.akim1784.util.GameWindow;
import scs.akim1784.util.ResourceManager;
import scs.akim1784.util.SaveManager;
import scs.akim1784.util.TileSize;
import it.marteEngine.ME;
import it.marteEngine.entity.PhysicsEntity;
import org.newdawn.slick.*;

/**
 * Player entity. Moves by clicking and dragging the mouse, similar to a slingshot.
 * Has a CoolDown meter used to indicate the time until the next movement possibility.
 * Collides with Wall entities, bounces off BounceWall entities, resets upon collision with Hazard entities,
 * gets points upon collision with Spawn entity, finishes level upon collision with Exit entity.
 * @see it.marteEngine.entity.Entity
 * @see it.marteEngine.entity.PhysicsEntity
 */

public class Player extends PhysicsEntity {

    private float moveSpeed= 0.1f;
    private int level;
    private boolean mouseExited=false;
    private int coolDown;
    private int mouseX;
    private int mouseY;
    public int score;
    public float shot;
    private float[] nextX;
    private float[] nextY;

    public Player(float x, float y, int level) {
        super(x, y);

        //initialize arrays used for trajectory drawing
        nextX=new float[10];
        nextY=new float[10];

        //convert level from 1->inf to 0->inf
        this.level=level-1;

        //set starting position
        startx=x;
        starty=y;

        //initialize score,scale,color
        score=0;
        shot=1;
        scale= TileSize.SCALE;
        this.setColor(Color.yellow);

        //set animations
        setGraphic(ResourceManager.getSpriteSheet("character"));
        duration=100;

        addAnimation(ME.WALK_RIGHT,true,0,0,1,2,3);
        addFlippedAnimation(ME.WALK_LEFT,true,true,false,0,0,1,2,3);
        setAnim(ME.WALK_RIGHT);

        //set hitBox
        setHitBox(TileSize.SCALE,6* TileSize.SCALE,14* TileSize.SCALE,10* TileSize.SCALE);

        //set physics variables
        maxSpeed.x=10;
        maxSpeed.y=10;

        friction.x=0.1f;
        friction.y=0.1f;

        gravity=0.2f;

        depth=110;

        coolDown = 0;

        //set type and bind mouse to command
        addType("PLAYER");

        bindToMouse("LAUNCH",Input.MOUSE_LEFT_BUTTON);
    }

    /**
     * Contains game logic, Player entity updates like movement and level exit.
     * @param container             GameContainer context
     * @param delta                 time
     * @throws SlickException       upon failed collision detection
     */
    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        super.update(container, delta);

        maxspeed(true,true);
        acceleration.x=0;
        acceleration.y=0;
        coolDown = Math.max(coolDown-1,0);

        if(pressed("LAUNCH") && coolDown == 0){
            acceleration.x=0;
            acceleration.y=0;
            speed.x=0;
            speed.y=0;
            container.pause();
            mouseX=container.getInput().getAbsoluteMouseX();
            mouseY=container.getInput().getAbsoluteMouseY();


            mouseExited=true;
        }

        if(!check("LAUNCH") && !container.isPaused() && mouseExited){
            mouseExited=false;

            int mouseX2=0;
            int mouseY2=0;

            if(!container.isPaused()) {

                mouseX2 = container.getInput().getAbsoluteMouseX();
                mouseY2 = container.getInput().getAbsoluteMouseY();
                if (mouseX2 < mouseX) {
                    setAnim(ME.WALK_RIGHT);
                }else if(mouseX < mouseX2){
                    setAnim(ME.WALK_LEFT);
                }
                speed.x=0;
                speed.y=0;
                acceleration.x = (mouseX-mouseX2)*moveSpeed;
                acceleration.y=(mouseY-mouseY2)*moveSpeed;

            }

            coolDown = 5*this.world.container.getFPS()/8;
            shot+=0.5f;
        }


        if(!check("LAUNCH") && container.isPaused()){
            container.resume();
        }


        if(collide("SPAWN",x,y)!=null){
            score+=collide("SPAWN",x,y).depth*100;
            collide("SPAWN",x,y).destroy();
        }


        if(collide("HAZARD",x,y)!=null){
            score=0;
            x=startx;
            y=starty;
        }

        if(collide("BOUNCE",x+Math.signum(speed.x),y)!=null){
            x+=Math.signum(speed.x);
            speed.x=-speed.x;
            acceleration.x=-acceleration.x;
        }


        if(collide("WALL",x,y+1)!=null && speed.y < 1){
            friction(true,false);
        }

        if(collide("EXIT", x, y)!=null){

            if(score > SaveManager.scores[level]) {
                SaveManager.scores[level] = score;
            }

            SaveManager.saveGame();
        }

    }

    /**
     * Contains Player entity rendering, trajectory and CoolDown bar rendering
     * @param container             GameContainer context
     * @param g                     Graphics
     * @throws SlickException       upon failure
     */
    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        super.render(container, g);

        if(container.isPaused()){
            drawTrajectory(container,g);
        }else if(coolDown > 0) {
            drawCoolDown(g);
        }
    }

    /**
     * Draw coolDown bar
     * @param g                     Slick2D Graphics component
     */
    private void drawCoolDown(Graphics g){
            int step = this.world.container.getFPS()/8;
            int k=0;
            for(int i=0; i<coolDown; i+=step){
                g.fillRect((150 + (10*k))*TileSize.SCALE, GameWindow.HEIGHT - (32*TileSize.SCALE),5*TileSize.SCALE,5*TileSize.SCALE);
                k++;
            }
    }

    /**
     * Draw approximate trajectory of Player entity
     * @param container             Game container
     * @param g                     Slick2D Graphics component
     */
    private void drawTrajectory(GameContainer container,Graphics g){
        int mouseX2=0;
        int mouseY2=0;
        mouseX2 = container.getInput().getAbsoluteMouseX();
        mouseY2 = container.getInput().getAbsoluteMouseY();

        float accX=(mouseX-mouseX2)*moveSpeed;
        float accY=(mouseY-mouseY2)*moveSpeed;

        nextX[0]=x+accX+(float)(hitboxWidth/2);
        nextY[0]=y+accY+(float)(hitboxHeight/2);
        accY = accY + gravity;
        container.getGraphics().fillOval(nextX[0],nextY[0],5*TileSize.SCALE,5*TileSize.SCALE);

        if(Math.abs(mouseX2-mouseX) > 1 && Math.abs(mouseY2-mouseY) > 1) {
            for (int i = 1; i < 10; i++) {
                nextX[i] = (nextX[i-1] + accX);
                nextY[i] = (nextY[i-1] + accY);
                accY = accY + gravity + friction.y;
                g.fillOval(nextX[i],nextY[i],5*TileSize.SCALE,5*TileSize.SCALE);
            }
        }
    }
}
