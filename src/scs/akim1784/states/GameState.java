package scs.akim1784.states;

import scs.akim1784.entities.Player;
import scs.akim1784.util.LevelLoader;
import scs.akim1784.util.TileSize;
import it.marteEngine.World;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;

/**
 * GameState class, in which a level and a Player entity are loaded, represents the actual game
 * @see it.marteEngine.World
 * @see org.newdawn.slick.state.BasicGameState
 */
public class GameState extends World {

    private Player player;
    private int level;

    public GameState(int id, GameContainer container, int level) {
        super(id, container);
        this.level=level;
    }

    /**
     * Load level based on level variable and add Player to scene
     * @param container             GameContainer context
     * @param game                  Current game
     * @throws SlickException       Upon failed initialization
     */
    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        super.init(container, game);

        try {
            LevelLoader.load("res/maps/level"+level+".json");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        LevelLoader.render(0,0,this,game);

        player=new Player(LevelLoader.startX*TileSize.SCALED_SIZE,LevelLoader.startY*TileSize.SCALED_SIZE,level);
        add(player);

    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        g.setAntiAlias(true);
        super.render(container, game, g);
        g.scale((float)TileSize.SCALE/2,(float)TileSize.SCALE/2);
        g.drawString("SCORE: " + player.score,20,10);
        g.resetTransform();

    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        super.update(container, game, delta);
    }

    /**
     *
     * @return  currently loaded level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Set level variable to currently loaded level. Used by LevelLoader class
     * @param level         level variable
     */
    public void setLevel(int level) {
        this.level = level;
    }
}
