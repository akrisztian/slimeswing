package scs.akim1784.states;

import scs.akim1784.entities.Button;
import scs.akim1784.util.*;
import it.marteEngine.World;
import it.marteEngine.entity.Entity;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;
import java.util.List;

/**
 * Level select screen where the user can start any of the already unlocked levels.
 * Contains buttons that change the current state to GameState.
 * @see it.marteEngine.World
 * @see org.newdawn.slick.state.BasicGameState
 */
public class LevelSelectState extends World {

    private StateBasedGame game;
    private GameState gameState;
    private boolean firstLoad;

    public LevelSelectState(int id, GameContainer container, StateBasedGame game) {
        super(id, container);
        this.game=game;
        firstLoad=true;

    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        super.render(container, game, g);
        g.scale((float)TileSize.SCALE/2,(float)TileSize.SCALE/2);
        g.drawString("TOTAL SCORE: " + SaveManager.totalScore,36,20);
        g.resetTransform();

        int i=0;
        int row=0;

        while (i < SaveManager.LEVEL_COUNT){
            for(int j=0; j<2; j++){
                i++;
                if(i>SaveManager.level){
                    ResourceManager.getImage("locked").draw(100* TileSize.SCALE+(j*100*TileSize.SCALE),100*TileSize.SCALE+(row*75*TileSize.SCALE),TileSize.SCALED_SIZE*6,TileSize.SCALED_SIZE*3);
                }
                if(i == SaveManager.LEVEL_COUNT){
                    break;
                }
            }
            row++;
        }

    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        super.init(container, game);

        try {
            LevelLoader.load("res/maps/menu.json");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        LevelLoader.render(0,0,this,game);

        add(new Button(32, GameWindow.HEIGHT-TileSize.SCALED_SIZE*3-32, ResourceManager.getImage("button"),"Back",-1));

        int i=0;
        int row=0;

        while (i < SaveManager.LEVEL_COUNT){
            for(int j=0; j<2; j++){
                i++;
                add(new Button(100* TileSize.SCALE+(j*100*TileSize.SCALE),100*TileSize.SCALE+(row*75*TileSize.SCALE), ResourceManager.getImage("button"),"Level " + i,i));
                if(i == SaveManager.LEVEL_COUNT){
                    break;
                }
            }
            row++;
        }

        bindToMouse("CLICK", Input.MOUSE_LEFT_BUTTON);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        super.update(container, game, delta);

        List<Entity> buttons = this.getEntities("BUTTON");
        int mX = container.getInput().getMouseX();
        int mY = container.getInput().getMouseY();

        for (Entity button : buttons) {
            if (mX >= button.x && mX <= button.x+button.hitboxWidth  && mY >= button.y && mY <= button.y + button.hitboxHeight) {
                if(pressed("CLICK") && button.depth <= SaveManager.level) {

                    if(button.depth == -1){
                        game.enterState(States.MENU);
                    }else {

                        if (firstLoad) {
                            firstLoad = false;
                            gameState = new GameState(States.GAME, game.getContainer(), button.depth);
                            game.addState(gameState);
                        } else {
                            gameState.setLevel(button.depth);
                            gameState.removeAll(gameState.getEntities());
                        }

                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }


                        game.enterState(States.GAME);
                        gameState.init(game.getContainer(), game);
                    }
                }
            }
        }
    }
}
