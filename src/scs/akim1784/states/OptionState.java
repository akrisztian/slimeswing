package scs.akim1784.states;

import scs.akim1784.entities.Button;
import scs.akim1784.entities.SwitchButton;
import scs.akim1784.util.*;
import it.marteEngine.World;
import it.marteEngine.entity.Entity;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;
import java.util.List;

/**
 * Option screen where the user can change basic settings.
 * Contains SwitchButtons to change settings, and a save delete button.
 * @see it.marteEngine.World
 * @see org.newdawn.slick.state.BasicGameState
 */
public class OptionState extends World {

    public OptionState(int id, GameContainer container) {
        super(id, container);
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        super.render(container, game, g);
        g.scale((float)TileSize.SCALE/2,(float)TileSize.SCALE/2);
        g.drawString("*For all changes to take effect please restart the game",36,20);
        g.resetTransform();

    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        super.init(container, game);

        try {
            LevelLoader.load("res/maps/menu.json");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


        LevelLoader.render(0,0,this,game);

        add(new Button(32, GameWindow.HEIGHT- TileSize.SCALED_SIZE*3-16*TileSize.SCALE, ResourceManager.getImage("button"),"Back",-1));
        add(new Button( (float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),75*TileSize.SCALE+55*TileSize.SCALE*4, ResourceManager.getImage("button"),"Delete Save",10));

        add(new SwitchButton( (float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),75*TileSize.SCALE,ResourceManager.getImage("button"),"Speed 1x",0,new String[]{"1x","2x"}));
        add(new SwitchButton( (float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),75*TileSize.SCALE+55*TileSize.SCALE,ResourceManager.getImage("button"),"Scale 2x",0,new String[]{"2x","1x"}));
        add(new SwitchButton( (float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),75*TileSize.SCALE+55*TileSize.SCALE*2,ResourceManager.getImage("button"),"VSync ON",0,new String[]{"ON","OFF"}));
        add(new SwitchButton( (float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),75*TileSize.SCALE+55*TileSize.SCALE*3,ResourceManager.getImage("button"),"Sound ON",0,new String[]{"ON","OFF"}));


        bindToMouse("CLICK", Input.MOUSE_LEFT_BUTTON);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        super.update(container, game, delta);

        List<Entity> buttons = this.getEntities("BUTTON");
        int mX = container.getInput().getMouseX();
        int mY = container.getInput().getMouseY();

        for (Entity button : buttons) {
            if (mX >= button.x && mX <= button.x+button.hitboxWidth  && mY >= button.y && mY <= button.y + button.hitboxHeight) {
                if(pressed("CLICK")) {
                    if (button.depth == -1) {
                        OptionsManager.saveOptions();
                        OptionsManager.reloadSettings();
                        game.enterState(States.MENU);
                    }else if(button.depth == 10){
                        try {
                            SaveManager.deleteSave();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

    }
}
