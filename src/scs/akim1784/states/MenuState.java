package scs.akim1784.states;

import org.newdawn.slick.font.effects.ColorEffect;
import scs.akim1784.entities.Button;
import scs.akim1784.util.*;
import it.marteEngine.World;
import it.marteEngine.entity.Entity;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;
import java.util.List;

/**
 * First screen of the game, where the user can select whether to play, set options, or quit the game.
 * Contains buttons, that change the current gameState
 * @see it.marteEngine.World
 * @see org.newdawn.slick.state.BasicGameState
 */
public class MenuState extends World {

    public MenuState(int id, GameContainer container) {
        super(id, container);
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        super.render(container, game, g);
        ResourceManager.getImage("logo").draw(100*TileSize.SCALE,50*TileSize.SCALE,(float)TileSize.SCALE/2);

        add(new Button((float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),150*TileSize.SCALE, ResourceManager.getImage("button"),"Play",States.LEVEL_SELECT));
        add(new Button((float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),220*TileSize.SCALE, ResourceManager.getImage("button"),"Options",States.OPTIONS));
        add(new Button((float)((GameWindow.WIDTH/2)-(TileSize.SCALED_SIZE*6)/2),290*TileSize.SCALE, ResourceManager.getImage("button"),"Exit",-1));

    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        super.init(container, game);

        try {
            LevelLoader.load("res/maps/menu.json");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        LevelLoader.render(0,0,this,game);

        bindToMouse("CLICK",Input.MOUSE_LEFT_BUTTON);

    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        super.update(container, game, delta);

        List<Entity> buttons = this.getEntities("BUTTON");
        int mX = container.getInput().getMouseX();
        int mY = container.getInput().getMouseY();

        for (Entity button : buttons) {
            if (mX >= button.x && mX <= button.x+button.hitboxWidth  && mY >= button.y && mY <= button.y + button.hitboxHeight) {
                if(pressed("CLICK")) {
                    if (button.depth == -1) {
                        SaveManager.saveGame();
                        container.setSoundOn(false);
                        container.exit();
                    } else {
                        game.enterState(button.depth);
                    }
                }
            }
        }
    }
}
