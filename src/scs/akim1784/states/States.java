package scs.akim1784.states;

/**
 * States class used to easily reference gameState IDs
 */
public class States {
    public static final int MENU=0;
    public static final int OPTIONS=1;
    public static final int LEVEL_SELECT=2;
    public static final int GAME=3;
}
